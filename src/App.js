import "./App.css";
import { useState, useEffect } from "react";
function App() {
  const [state, setState] = useState(localStorage.getItem('note'));
  function setNote(e) {
    setState(e.target.value)
    localStorage.setItem('note', state)
  }
  function clearNote() {
    document.querySelector('.textarea.is-large').value = '';
    localStorage.setItem('note', '')
    setState('')
  }
  useEffect(() => {
    document.querySelector('.textarea.is-large').value = localStorage.getItem('note');
  }, [])
  return (
    <div className="App">
      <div className="box">
        <div className="field">
          <div className="control">
            <textarea className="textarea is-large" placeholder="Notes..." onChange={setNote} value={state} />
          </div>
        </div>
        <button className="button is-large is-primary is-active" onClick={setNote}>Save</button>
        <button className="button is-large" onClick={clearNote}>Clear</button>
      </div>
    </div>
  );
}

export default App;
